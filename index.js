// return greater than cirteria
db.users.find({ age: { $gt: 68 } });
// return greater or equal than cirteria
db.users.find({ age: { $gte: 65 } });

// returs less than criteria
db.users.find({ age: { $lt: 68 } });
// return less or equal than cirteria
db.users.find({ age: { $lte: 68 } });

// returns documents not equal to given criteria
db.users.find({ age: { $ne: 82 } });

// returns all that have the matching criteria inside the array
db.users.find({ lastName: { $in: ["Doe", "Hawking"] } });
db.users.find({ courses: { $in: ["HTML", "React"] } });

// Logical Query Operators----------

// returns what matches whatever is in criteria
db.users.find({ $or: [{ firstName: "Niel" }, { age: 25 }] });
db.users.find({ $or: [{ firstName: "Niel" }, { age: { $gt: 30 } }] });

// returns what matches all what is in the criteria
db.users.find({ $and: [{ firstName: "Niel" }, { age: { $gt: 30 } }] });
db.users.find({ $and: [{ firstName: "Niel" }, { age: 30 }] });

// Field Projection
// cannot combine filed projection and exclusion in one command _id is an exception to this rule
// returns only the fields you wan to see (set field to 1 or true)
db.users.find(
  { firstName: "Jane" },
  {
    firstName: 1,
    lastName: 1,
    age: 1,
  }
);

db.users.find(
  { $and: [{ firstName: "Niel" }, { age: { $gt: 30 } }] },
  {
    contact: 1,
  }
);

// Exclusion
// cannot combine filed projection and exclusion in one command _id is an exception to this rule
// returns all fields except what you set as 0 or false
db.users.find(
  { firstName: "Jane" },
  {
    firstName: 0,
    lastName: 0,
  }
);

// return specific fields in embeded documents
// need "" because JSON format
//
db.users.find(
  { firstName: "Jane" },
  {
    "contact.phone": 1,
    _id: 0,
  }
);

// Evaluation Query Operators
// returns all that containt $regex in a given criteria, $option: "$i" ignorse upper/lower case
db.users.find({ firstName: { $regex: "N", $options: "$i" } });

// CRUD operations
db.users.updateOne(
  { age: { $lte: 17 } },
  {
    $set: {
      firstName: "Chris",
      lastName: "Mortel",
    },
  }
);

db.users.deleteOne(
  { $and: [{ firstName: "Chris" }, { lastName: "Mortel" }] },
  {
    contact: 1,
  }
);
